<?php

class Toptal_Weather_InfoController extends Mage_Core_Controller_Front_Action
{
    const ERROR_MESSAGE_GET_WEATHER_BY_CITY = 'We were unable to fetch weather information for this city.';

    public function checkPostAction()
    {
        $data = $this->getRequest()->getPost();
        if($city = $data['city']) {
            $helper = Mage::helper('toptal_weather');
            try {
                /** @var Toptal_Weather_Model_Service $service */
                $service = Mage::getModel('toptal_weather/service');
                $weatherData = $service->getWeatherByCity($city);

                /** @var Toptal_Weather_Helper_Data $helper */
                $entry = $helper->createHistoryEntry($weatherData, $this->_getCustomerId());

                /** redirect to show weather page */
                $this->_redirect('toptal_weather/info/show', array('id' => $entry->getId()));
            } catch(Exception $e) {
                /** redirect user to previous page with error about fetching weather info */
                $session = Mage::getModel('core/session');
                $session->addWarning($helper->__(self::ERROR_MESSAGE_GET_WEATHER_BY_CITY));
                $this->_redirect('/');
            }
        }
    }

    /**
     * AJAX action to fetch weather data
     * for a given city
     *
     * @return void
     */
    public function checkAjaxAction()
    {
        $data = $this->getRequest()->getPost();
        if($city = $data['city']) {
            try {
                /** @var Toptal_Weather_Model_Service $service */
                $service = Mage::getModel('toptal_weather/service');
                $weatherData = $service->getWeatherByCity($city);

                /** @var Toptal_Weather_Helper_Data $helper */
                $helper = Mage::helper('toptal_weather');
                $helper->createHistoryEntry($weatherData, $this->_getCustomerId());

                /** send response with weather data */
                $this->getResponse()->setHeader('Content-type', 'application/json');
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
                    'weather_data' => $weatherData->getData(),
                    'error' => false,
                )));
            } catch(Exception $e) {
                /** send response with error message */
                $this->getResponse()->setHeader('Content-type', 'application/json');
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
                    'error' => true,
                    'message' => Mage::helper('toptal_weather')->__(self::ERROR_MESSAGE_GET_WEATHER_BY_CITY)
                )));
            }
        }
    }

    public function showAction()
    {
        $id = $this->getRequest()->getParam('id');
        if($id) {
            /** @var Toptal_Weather_Model_History $weather */
            $weather = Mage::getModel('toptal_weather/history')->load($id);
            /** @var Toptal_Weather_Helper_Data $helper */
            $helper = Mage::helper('toptal_weather');
            if($weather->getId() && $helper->historyBelongsToCurrentCustomer($weather)) {
                Mage::register('current_weather', $weather);
                $this->loadLayout();
                $this->renderLayout();
            } else {
                $this->_redirect('/');
            }
        } else {
            $this->_redirect('/');
        }
    }

    public function listAction()
    {
        $filters = array(
            'date_from' => $this->getRequest()->getParam('from'),
            'date_to' => $this->getRequest()->getParam('to'),
            'city' => $this->getRequest()->getParam('city')
        );

        $helper = Mage::helper('toptal_weather');
        $collection = $helper->getWeatherHistoryForCurrentCustomer($filters);

        Mage::register('weather_history', $collection);
        Mage::register('weather_history_filters', $filters);

        $this->loadLayout();
        $this->renderLayout();
    }

    public function pdfAction()
    {
        $id = $this->getRequest()->getParam('id');
        if($id) {
            /** @var Toptal_Weather_Model_History $weather */
            $weather = Mage::getModel('toptal_weather/history')->load($id);
            /** @var Toptal_Weather_Helper_Data $helper */
            $helper = Mage::helper('toptal_weather');
            if($weather->getId() && $helper->historyBelongsToCurrentCustomer($weather)) {
                header('Content-type: application/pdf');
                echo $helper->weatherToPdf($weather)->render(); die();
            } else {
                $this->_redirect('/');
            }
        } else {
            $this->_redirect('/');
        }
    }

    protected function _getCustomerId()
    {
        $customer = Mage::getModel('customer/session')->getCustomer();
        $customerId = null;
        if($id = $customer->getId()) {
            $customerId = $id;
        }
        return $customerId;
    }
}
<?php

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$table = $installer->getConnection()->newTable($installer->getTable('toptal_weather/history'))
    ->addColumn('history_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'identity' => true,
    ), 'Note ID')
    ->addColumn('city', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable' => false,
        'default' => '',
    ), 'Title')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => true,
        'default' => null,
    ), 'Content')
    ->addColumn('ip_address', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable' => false,
    ), 'Created Date')
    ->addColumn('wind_speed', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => true,
        'default' => null,
    ), 'Wind Speed')
    ->addColumn('temperature', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => true,
        'default' => null,
    ), 'Temperature')
    ->addColumn('min_temperature', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => true,
        'default' => null,
    ), 'Min Temperature')
    ->addColumn('max_temperature', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => true,
        'default' => null,
    ), 'Max Temperature')
    ->addColumn('pressure', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => true,
        'default' => null,
    ), 'Pressure')
    ->addColumn('humidity', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => true,
        'default' => null,
    ), 'humidity')
    ->addColumn('visibility', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => true,
        'default' => null,
    ), 'Visibility')
    ->addColumn('weather_icons', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => true,
        'default' => null,
    ), 'Serialized Weather Icons URLs')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
        'nullable' => false,
    ), 'Created At');

$installer->getConnection()->createTable($table);
$installer->endSetup();
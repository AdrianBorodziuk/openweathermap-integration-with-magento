<?php

class Toptal_Weather_Block_Weather_List extends Mage_Core_Block_Template
{
    protected $_history;

    public function getWeatherHistory()
    {
        if($history = Mage::registry('weather_history')) {
            return $history;
        }
        return false;
    }

    public function getShowUrl($id)
    {
        return Mage::getUrl('toptal_weather/info/show', array('id' => $id));
    }

    public function getClearUrl()
    {
        return Mage::getUrl('toptal_weather/info/list');
    }

    public function getCurrentDate()
    {
        return Mage::getModel('core/date')->date('Y-m-d');
    }

    public function getCityPlaceholder()
    {
        if($filter = $this->getFilterApplied('city')) {
            return $filter;
        }
        return '';
    }

    public function getDateFromPlaceholder()
    {
        if($filter = $this->getFilterApplied('date_from')) {
            return $filter;
        }
        return $this->getCurrentDate();
    }

    public function getDateToPlaceholder()
    {
        if($filter = $this->getFilterApplied('date_to')) {
            return $filter;
        }
        return $this->getCurrentDate();
    }

    public function getFilterApplied($field)
    {
        $filters = Mage::registry('weather_history_filters');
        if(array_key_exists($field, $filters)) {
            return $filters[$field];
        }
        return false;
    }
}
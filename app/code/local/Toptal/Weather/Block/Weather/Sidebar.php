<?php

class Toptal_Weather_Block_Weather_Sidebar extends Mage_Core_Block_Template
{
    public function getActionUrl()
    {
        return Mage::getUrl('toptal_weather/info/checkPost');
    }

    public function getHistoryUrl()
    {
        return Mage::getUrl('toptal_weather/info/list');
    }
}
<?php

class Toptal_Weather_Block_Weather_Show extends Mage_Core_Block_Template
{
    public function getWeather()
    {
        return Mage::registry('current_weather');
    }

    public function getBackUrl()
    {
        return Mage::getUrl('toptal_weather/info/list');
    }

    public function getPdfUrl()
    {
        return Mage::getUrl('toptal_weather/info/pdf', array('id' => $this->getWeather()->getId()));
    }
}
<?php

class Toptal_Weather_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_pdfFieldsToSkip = array(
        'customer_id',
        'ip_address',
        'history_id',
        'weather_icons',
        'city',
        'created_at'
    );

    public function createHistoryEntry(Varien_Object $weather, $customerId)
    {
        $history = Mage::getModel('toptal_weather/history');
        $history->setData(array(
            'city' => $weather->getCity(),
            'customer_id' => $customerId,
            'ip_address' => $this->_getCustomerIpAddress(),
            'wind_speed' => $weather->getWindSpeed(),
            'pressure' => $weather->getPressure(),
            'temperature' => $weather->getTemperature(),
            'min_temperature' => $weather->getMinTemperature(),
            'max_temperature' => $weather->getMaxTemperature(),
            'humidity' => $weather->getHumidity(),
            'visibility' => $weather->getVisibility(),
            'weather_icons' => serialize($weather->getWeatherIcons())
        ));

        $history->save();
        return $history;
    }

    public function historyBelongsToCurrentCustomer(Toptal_Weather_Model_History $history)
    {
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/session')->getCustomer();
        $currentIp = $this->_getCustomerIpAddress();

        if($currentIp == $history->getIpAddress() ||
          ($customer->getId() && ($customer->getId() == $history->getCustomerId())))
        {
            return true;
        }

        return false;
    }

    public function getWeatherHistoryForCurrentCustomer($filters)
    {
        $customer = Mage::getModel('customer/session')->getCustomer();
        $collection = Mage::getModel('toptal_weather/history')
            ->getCollection()
            ->addFieldToFilter(
                array('customer_id', 'ip_address'),
                array(
                    array('eq' => $customer->getId()),
                    array('eq' => $this->_getCustomerIpAddress())
                )
            );

        foreach($filters as $key => $value) {
            if($value && !empty($value)) {
                if($key == 'city') {
                    $collection->addFieldToFilter('city', $value);
                } else if($key == 'date_from') {
                    $collection->addFieldToFilter('created_at', array('gteq' => $value));
                } else if($key == 'date_to') {
                    $collection->addFieldToFilter('created_at', array('lteq' => $value));
                }
            }
        }

        return $collection;
    }

    public function weatherToPdf(Toptal_Weather_Model_History $weather)
    {
        $pdf = new Zend_Pdf();
        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES);
        $height = $page->getHeight() - 30;
        $margin = 20;

        $page->setFont($font, 18);
        $page->drawText("Weather for {$weather->getCity()} - {$weather->getCreatedAt()}", $margin, $height);
        $height -= 10;
        $margin += 40;

        $page->setFont($font, 14);
        foreach($weather->getData() as $key => $value) {
            if(!in_array($key, $this->_pdfFieldsToSkip)) {
                $height -=20;
                $text = ucwords(str_replace('_', ' ', $key)) . ': ' . $value;
                $page->drawText($text, $margin, $height);
            }
        }

        $pdf->pages[] = $page;
        return $pdf;
    }

    public function _getCustomerIpAddress()
    {
        $ipaddress = null;
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');

        return $ipaddress;
    }
}
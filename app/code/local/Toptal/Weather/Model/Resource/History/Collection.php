<?php

class Toptal_Weather_Model_Resource_History_Collection
    extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('toptal_weather/history');
    }
}
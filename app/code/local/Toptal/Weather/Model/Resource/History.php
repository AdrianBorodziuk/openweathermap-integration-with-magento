<?php

class Toptal_Weather_Model_Resource_History extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('toptal_weather/history', 'history_id');
    }
}
<?php

class Toptal_Weather_Model_Api_Adapter_OpenWeatherMap
{
    /**
     * API URL for fetching weather data by city
     */
    const URL_WEATHER_BY_CITY = 'http://api.openweathermap.org/data/2.5/weather?q={city_name}&APPID={api_key}';

    /**
     * Timeout in seconds while trying to connect
     */
    const CONNECTION_TIME_OUT = 5;

    /**
     * Timeout in seconds for cURL execute
     */
    const TIMEOUT = 10;

    /**
     * Service API key
     *
     * @var string
     */
    protected $_apiKey;

    /**
     * City name
     *
     * @var string
     */
    protected $_city;

    /**
     * Curl handle
     *
     * @var object
     */
    protected $_curl;

    /**
     * API response array
     *
     * @var stdClass
     */
    protected $_response;

    /**
     * Call OpenWeatherMap API
     *
     * @throws Exception
     */
    public function call()
    {
        if(!$this->_city) {
            Mage::throwException('No city specified.');
        } else if(!$this->_apiKey) {
            Mage::throwException('No API key specified');
        }

        $this->_buildRequest();
        $this->_sendRequest();
    }

    /**
     * Set city for API call
     *
     * @param string $city
     */
    public function setCity($city)
    {
        if(!is_string($city) || empty($city)) {
            Mage::throwException('Invalid city name.');
        }

        $this->_city = $city;
    }

    /**
     * Set API key
     *
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        if(!is_string($apiKey) || empty($apiKey)) {
            Mage::throwException('Invalid API key.');
        }

        $this->_apiKey = $apiKey;
    }

    /**
     * Fetch API response
     *
     * @return stdClass
     */
    public function getResponse()
    {
        return $this->_response;
    }

    protected function _buildRequest()
    {
        $this->_curl = curl_init();

        curl_setopt_array($this->_curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => self::CONNECTION_TIME_OUT,
            CURLOPT_TIMEOUT => self::TIMEOUT,
            CURLOPT_URL => $this->_getWeatherByCityUrl(
                urlencode($this->_city)
            )
        ));
    }

    protected function _sendRequest()
    {
        $response = curl_exec($this->_curl);
        curl_close($this->_curl);

        if(!$response) {
            Mage::throwException('Failed to retrieve response.');
        }

        $this->_response = json_decode($response);
    }

    protected function _getWeatherByCityUrl($city)
    {
        $url = str_replace('{city_name}', $city, self::URL_WEATHER_BY_CITY);

        return str_replace('{api_key}', $this->_apiKey, $url);
    }
}
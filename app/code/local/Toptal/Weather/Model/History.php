<?php

class Toptal_Weather_Model_History extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('toptal_weather/history');
    }

    protected function _beforeSave()
    {
        $timestamp = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        if(!$this->getCreatedAt()) {
            $this->setCreatedAt($timestamp);
        }
        parent::_beforeSave();
    }

    public function getIcons()
    {
        if(!$this->getId()) {
            Mage::throwException('Model not loaded.');
        }
        return unserialize($this->getWeatherIcons());
    }
}
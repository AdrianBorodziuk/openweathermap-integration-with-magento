<?php

class Toptal_Weather_Model_Service extends Mage_Core_Model_Abstract
{
    const XML_PATH_API_KEY = 'weather/settings/api_key';

    /**
     * Open Weather Map API Adapter
     *
     * @var Toptal_Weather_Model_Api_Adapter_OpenWeatherMap
     */
    protected $_apiAdapter;

    /**
     * Get weather data by given city
     *
     * @param string $city
     * @throws Exception
     * @return Toptal_Weather_Model_Weather_Wrapper
     */
    public function getWeatherByCity($city)
    {
        /** @var Toptal_Weather_Model_Api_Adapter_OpenWeatherMap $api */
        $api = $this->_getApiAdapter();
        $api->setCity($city);

        $api->call();
        return $this->_wrapResponse($api->getResponse());
    }

    protected function _wrapResponse(stdClass $apiResponse)
    {
        /** @var Toptal_Weather_Model_Weather_Wrapper $wrapper */
        $wrapper = Mage::getModel('toptal_weather/weather_wrapper');
        $wrapper->importData($apiResponse);

        return $wrapper;
    }

    protected function _getApiAdapter()
    {
        if(!$this->_apiAdapter) {
            $adapter = Mage::getModel('toptal_weather/api_adapter_openWeatherMap');
            $adapter->setApiKey(Mage::getStoreConfig(self::XML_PATH_API_KEY));
            $this->_apiAdapter =  $adapter;
        }

        return $this->_apiAdapter;
    }
}
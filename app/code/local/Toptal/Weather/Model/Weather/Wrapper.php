<?php

class Toptal_Weather_Model_Weather_Wrapper extends Varien_Object
{
    const ICON_URL = 'http://openweathermap.org/img/w/{icon}.png';

    /**
     * Original API response array
     *
     * @var stdClass
     */
    protected $_origData;

    public function importData(stdClass $apiResponse)
    {
        $this->_origData = $apiResponse;

        $this->setCity($apiResponse->name);
        $this->setWindSpeed($apiResponse->wind->speed);
        $this->setTemperature($apiResponse->main->temp);
        $this->setMinTemperature($apiResponse->main->temp_min);
        $this->setMaxTemperature($apiResponse->main->temp_max);
        $this->setPressure($apiResponse->main->pressure);
        $this->setHumidity($apiResponse->main->humidity);
        $this->setVisibility($apiResponse->visibility);

        $icons = array();
        $usedIcons = array();
        foreach($apiResponse->weather as $info) {
            if(!in_array($info->icon, $usedIcons)) {
                $usedIcons[] = $info->icon;
                $icons[$info->main] = str_replace("{icon}", $info->icon, self::ICON_URL);
            }
        }
        $this->setWeatherIcons($icons);

        return $this;
    }
}